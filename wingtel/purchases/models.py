from django.contrib.auth.models import User
from django.db import models
from model_utils import Choices


class Purchase(models.Model):
    """Represents a purchase for a user and their subscription(s)"""
    STATUS = Choices(
        ('pending', 'Pending'),
        ('overdue', 'Past Due'),
        ('complete', 'Complete'),
        ('cancelled', 'Cancelled'),
    )

    user = models.ForeignKey(
        User, related_name='purchases',
        on_delete=models.PROTECT
    )
    att_subscription = models.ForeignKey(
        'subscriptions.ATTSubscription', related_name='purchases',
        on_delete=models.PROTECT,
        null=True, blank=True
    )
    sprint_subscription = models.ForeignKey(
        'subscriptions.SprintSubscription', related_name='purchases',
        on_delete=models.PROTECT,
        null=True, blank=True
    )

    status = models.CharField(max_length=20, choices=STATUS, default=STATUS.pending)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    payment_date = models.DateTimeField(null=True, db_index=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
