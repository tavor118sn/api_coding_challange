from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from wingtel.purchases.models import Purchase
from .constants import SUBSCRIPTIONS
from .validators import validate_subscription_activation


class ActivateSubscriptionModelMixin():
    """Mixin for subscriptions models to provide `activate` action."""

    def activate_subscription(self):
        now = timezone.now()
        amount = self.plan.price

        # get appropriate subscription type
        subscription_model = SUBSCRIPTIONS[self.__class__.__name__]

        # prepare data to create purchase
        purchase_data = {
            'user': self.user,
            'amount': amount,
            'status': Purchase.STATUS.overdue,
            'payment_date': now,
            subscription_model: self
        }

        with transaction.atomic():
            Purchase.objects.create(**purchase_data)

            self.status = self.STATUS.active
            self.effective_date = now
            self.save()

        return self


class ActivateSubscriptionViewSetMixin:
    """
    Mixin for subscription ViewSets to provide `activate` action.
    """

    @action(detail=True, methods=['get'])
    def activate(self, request, pk=None):
        subscription = self.get_object()
        try:
            validate_subscription_activation(subscription)
        except ValidationError as e:
            return Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)

        subscription = subscription.activate_subscription()

        serializer = self.get_serializer(subscription)
        return Response(serializer.data)
