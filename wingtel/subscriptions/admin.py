from django.contrib import admin

from .models import SprintSubscription, ATTSubscription


@admin.register(SprintSubscription)
class SprintSubscriptionAdmin(admin.ModelAdmin):
    pass


@admin.register(ATTSubscription)
class ATTSubscriptionAdmin(admin.ModelAdmin):
    pass
