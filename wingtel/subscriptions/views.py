from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from .mixins import ActivateSubscriptionViewSetMixin
from .models import ATTSubscription
from .models import SprintSubscription
from .serializers import ATTSubscriptionSerializer
from .serializers import SprintSubscriptionSerializer


class SprintSubscriptionViewSet(ActivateSubscriptionViewSetMixin, ModelViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = SprintSubscriptionSerializer

    def get_queryset(self):
        return SprintSubscription.objects.filter(
            user=self.request.user,
        )


class ATTSubscriptionViewSet(ActivateSubscriptionViewSetMixin, ModelViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ATTSubscriptionSerializer

    def get_queryset(self):
        return ATTSubscription.objects.filter(
            user=self.request.user,
        )
