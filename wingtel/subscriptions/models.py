from django.contrib.auth.models import User
from django.db import models
from model_utils import Choices

from wingtel.plans.models import Plan

from wingtel.subscriptions.mixins import ActivateSubscriptionModelMixin


class SprintSubscription(models.Model, ActivateSubscriptionModelMixin):
    """Represents a subscription with Sprint for a user and a single device"""
    STATUS = Choices(
        ('new', 'New'),
        ('active', 'Active'),
        ('suspended', 'Suspended'),
        ('expired', 'Expired'),
    )

    # Owning user
    user = models.ForeignKey(
        User, related_name='sprint_subscriptions',
        on_delete=models.PROTECT
    )

    plan = models.ForeignKey(
        Plan, related_name='sprint_subscriptions',
        null=True, on_delete=models.PROTECT
    )
    status = models.CharField(max_length=10, choices=STATUS, default=STATUS.new)

    device_id = models.CharField(max_length=20, blank=True, default='')
    phone_number = models.CharField(max_length=20, blank=True, default='')
    phone_model = models.CharField(max_length=128, blank=True, default='')

    sprint_id = models.CharField(max_length=16, null=True)

    effective_date = models.DateTimeField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Sprint Subscription'
        verbose_name_plural = 'Sprint Subscriptions'
        ordering = ['-id']

    def __str__(self):
        return f'{self.user.username}\'s Sprint Subscription (id:{self.id})'


class ATTSubscription(models.Model, ActivateSubscriptionModelMixin):
    """Represents a subscription with AT&T for a user and a single device"""
    STATUS = Choices(
        ('new', 'New'),
        ('active', 'Active'),
        ('expired', 'Expired'),
    )

    # Owning user
    user = models.ForeignKey(
        User, related_name='att_subscriptions',
        on_delete=models.PROTECT,
    )

    plan = models.ForeignKey(
        Plan, related_name='att_subscriptions',
        null=True, on_delete=models.PROTECT)
    status = models.CharField(max_length=10, choices=STATUS, default=STATUS.new)

    device_id = models.CharField(max_length=20, blank=True, default='')
    phone_number = models.CharField(max_length=20, blank=True, default='')
    phone_model = models.CharField(max_length=128, blank=True, default='')
    network_type = models.CharField(max_length=5, blank=True, default='')

    effective_date = models.DateTimeField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'ATT Subscription'
        verbose_name_plural = 'ATT Subscriptions'
        ordering = ['-id']

    def __str__(self):
        return f'{self.user.username}\'s ATT Subscription (id:{self.id})'
