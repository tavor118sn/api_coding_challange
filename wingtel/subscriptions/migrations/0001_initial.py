# Generated by Django 2.2.1 on 2019-12-19 07:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import wingtel.subscriptions.mixins


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('plans', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SprintSubscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('new', 'New'), ('active', 'Active'), ('suspended', 'Suspended'), ('expired', 'Expired')], default='new', max_length=10)),
                ('device_id', models.CharField(blank=True, default='', max_length=20)),
                ('phone_number', models.CharField(blank=True, default='', max_length=20)),
                ('phone_model', models.CharField(blank=True, default='', max_length=128)),
                ('sprint_id', models.CharField(max_length=16, null=True)),
                ('effective_date', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('plan', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='sprint_subscriptions', to='plans.Plan')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='sprint_subscriptions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Sprint Subscription',
                'verbose_name_plural': 'Sprint Subscriptions',
                'ordering': ['-id'],
            },
            bases=(models.Model, wingtel.subscriptions.mixins.ActivateSubscriptionModelMixin),
        ),
        migrations.CreateModel(
            name='ATTSubscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('new', 'New'), ('active', 'Active'), ('expired', 'Expired')], default='new', max_length=10)),
                ('device_id', models.CharField(blank=True, default='', max_length=20)),
                ('phone_number', models.CharField(blank=True, default='', max_length=20)),
                ('phone_model', models.CharField(blank=True, default='', max_length=128)),
                ('network_type', models.CharField(blank=True, default='', max_length=5)),
                ('effective_date', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('plan', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='att_subscriptions', to='plans.Plan')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='att_subscriptions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'ATT Subscription',
                'verbose_name_plural': 'ATT Subscriptions',
                'ordering': ['-id'],
            },
            bases=(models.Model, wingtel.subscriptions.mixins.ActivateSubscriptionModelMixin),
        ),
    ]
