from django.core.exceptions import ValidationError


def validate_subscription_activation(subscription):
    """
    Validate if subscription can be activated.
    """
    if subscription.status != subscription.STATUS.new:
        raise ValidationError(
            'You can activate only subscription with `new` status.'
        )

    # Checks required subscription data is present before activation,
    # including plan, phone_number, device_id
    required_subscription_fields = ('plan', 'phone_number', 'device_id')
    for field in required_subscription_fields:
        if not getattr(subscription, field):
            raise ValidationError(
                f'Field `{field}` is required to activate subscription.'
            )
