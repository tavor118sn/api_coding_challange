from rest_framework import serializers

from .models import ATTSubscription
from .models import SprintSubscription


class SprintSubscriptionSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )

    class Meta:
        model = SprintSubscription

        fields = [
            'id', 'user', 'plan', 'status',
            'device_id', 'phone_number', 'phone_model',
            'sprint_id', 'effective_date',
        ]

        read_only_fields = ('status', 'effective_date',)


class ATTSubscriptionSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )

    class Meta:
        model = ATTSubscription

        fields = [
            'id', 'user', 'plan', 'status',
            'device_id', 'phone_number', 'phone_model',
            'network_type', 'effective_date',
        ]

        read_only_fields = ('status', 'effective_date',)
