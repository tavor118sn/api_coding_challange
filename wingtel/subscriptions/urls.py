from rest_framework import routers

from . import views

router = routers.DefaultRouter()

router.register(
    'subsrciptions/sprint_subscriptions',
    views.SprintSubscriptionViewSet,
    base_name='sprint_subscriptions'
)

router.register(
    'subsrciptions/att_subscriptions',
    views.ATTSubscriptionViewSet,
    base_name='att_subscriptions'
)
