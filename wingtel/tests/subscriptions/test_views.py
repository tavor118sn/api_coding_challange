from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse

from wingtel.plans.models import Plan
from wingtel.subscriptions.models import SprintSubscription, ATTSubscription
from wingtel.tests.base import BaseAPITestCaseSetup

User = get_user_model()


class SprintSubscriptionListViewSetTests(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.url = reverse('api:sprint_subscriptions-list')

    def test_get_page_without_authentication(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_page_with_authentication(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class SprintSubscriptionDetailViewSetTests(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.subscription = SprintSubscription.objects.create(
            user=self.user,
            device_id=12345678,
            phone_number=777,
            phone_model='iPhone',
        )
        self.url = reverse(
            'api:sprint_subscriptions-detail',
            kwargs={'pk': self.subscription.id}
        )

    def test_get_page_without_authentication(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_page_with_authentication(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_activate_subscription(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ActivateSprintSubscriptionViewSetTests(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.plan = Plan.objects.create(
            name='1GB',
            price=15,
            data_available=1024
        )

        self.subscription = SprintSubscription.objects.create(
            user=self.user,
            plan=self.plan,
            device_id=12345678,
            phone_number=777,
            phone_model='iPhone',
        )
        self.url = reverse(
            'api:sprint_subscriptions-activate',
            kwargs={'pk': self.subscription.id}
        )

    def test_get_page_without_authentication(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_activate_subscription(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ATTSubscriptionListViewSetTests(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.url = reverse('api:att_subscriptions-list')

    def test_get_page_without_authentication(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_page_with_authentication(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ATTSubscriptionDetailViewSetTests(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.subscription = ATTSubscription.objects.create(
            user=self.user,
            device_id=12345678,
            phone_number=777,
            phone_model='iPhone',
        )
        self.url = reverse(
            'api:att_subscriptions-detail',
            kwargs={'pk': self.subscription.id}
        )

    def test_get_page_without_authentication(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_page_with_authentication(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_activate_subscription(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ActivateATTSubscriptionViewSetTests(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.plan = Plan.objects.create(
            name='1GB',
            price=15,
            data_available=1024
        )

        self.subscription = ATTSubscription.objects.create(
            user=self.user,
            plan=self.plan,
            device_id=12345678,
            phone_number=777,
            phone_model='iPhone',
        )
        self.url = reverse(
            'api:att_subscriptions-activate',
            kwargs={'pk': self.subscription.id}
        )

    def test_get_page_without_authentication(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_activate_subscription(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
