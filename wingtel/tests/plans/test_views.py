from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse

from wingtel.tests.base import BaseAPITestCaseSetup

User = get_user_model()


class PlanViewSetTests(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.url = reverse('api:plan-list')

    def test_get_page_without_authentication(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_page_with_authentication(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
