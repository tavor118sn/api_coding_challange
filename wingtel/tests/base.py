from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase

User = get_user_model()


class BaseAPITestCaseSetup(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            username='test_user@example.com',
            password='test12345',
        )
