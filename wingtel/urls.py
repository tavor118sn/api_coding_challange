"""wingtel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import path
from django.views.generic import RedirectView
from rest_framework import routers

from wingtel.plans.views import PlanViewSet
from wingtel.purchases.views import PurchaseViewSet
from wingtel.subscriptions.urls import router as subscription_router

router = routers.DefaultRouter()

router.register(r'plans', PlanViewSet)
router.register(r'purchases', PurchaseViewSet)

router.registry.extend(subscription_router.registry)

urlpatterns = [
    path('', RedirectView.as_view(url='api/v1/')),

    path('admin/', admin.site.urls),

    # API
    path('api/v1/', include((router.urls, 'api'), namespace='api')),

    # authentication
    path('rest-auth/', include('rest_auth.urls'))
]
