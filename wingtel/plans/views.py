from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from wingtel.plans.models import Plan
from wingtel.plans.serializers import PlanSerializer


class PlanViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.
    """
    permission_classes = (IsAuthenticated,)
    queryset = Plan.objects.all()
    serializer_class = PlanSerializer
